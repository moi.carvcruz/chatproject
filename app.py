import transformers
import torch.nn as nn
from torch.nn import CrossEntropyLoss
from flask import Flask, jsonify, request, render_template, url_for
import pandas as pd
import torch                            
from transformers import BertConfig, BertModel, BertPreTrainedModel, BertTokenizer
from arquitectura import model_factory, weights_load, model_inference, data_cleansing, db_value
import language_tool_python
import pyrebase
import json

tool = language_tool_python.LanguageTool('es')
t_model, tokenizer = weights_load('./Modelo_p', cuda = False)
with open('config_data.json') as file:
    data = json.load(file)
firebase = pyrebase.initialize_app(data)
database = firebase.database()
db=pd.read_excel("db.xlsx")

app= Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def home():
    return render_template('home.html')

@app.route("/getanswer", methods=["POST"])
def getanswer(tokenizer=tokenizer,t_model=t_model,database=database,tool=tool):
    json_doc=request.get_json(force=True)
    sentence=json_doc.get('oracion')
    sentence = data_cleansing(sentence,tool)
    etiqueta = model_inference(sentence, tokenizer, t_model)
    #---------------------------------------------------------------------------
    value = db_value(etiqueta,database)
    # value=db['respuesta'][etiqueta]
    # if etiqueta==0:
    #   v_f=[]
    #   for i in range(1,len(value)):
    #     v_f.append(db['respuesta'][i])
    #   value=v_f
    resp={"respuesta":value}

    return jsonify(resp)

@app.route("/getlabel", methods=["POST"])
def getlabel(tokenizer=tokenizer,t_model=t_model,tool=tool):
    json_doc=request.get_json(force=True)
    sentence=json_doc.get('oracion')
    sentence = data_cleansing(sentence,tool)
    etiqueta = model_inference(sentence, tokenizer, t_model)
    resp={"etiqueta":etiqueta}

    return jsonify(resp)

@app.route("/temas")
def temas(db=db):
    lista_temas=db['tema']
    lista_etiqueta=db['etiqueta']
    return render_template("temas.html", len = len(lista_etiqueta), lista_etiqueta = lista_etiqueta, lista_temas = lista_temas) 

@app.route("/respuestas")
def respuestas(db=db):
    lista_temas=db['tema']
    lista_respuesta=db['respuesta']
    return render_template("temas.html", len = len(lista_temas), lista_etiqueta = lista_respuesta, lista_temas = lista_temas)

if __name__ == '__main__':
    app.run(debug=True)